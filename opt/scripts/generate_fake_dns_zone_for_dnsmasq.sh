#!/bin/sh

usage() {
    cat <<EOF >&2
$@
Usage: $0 
Use $0 to automatically write dns fake hosts file
( values are fetched from /etc/default/pxe-parinux )
EOF
    exit 1
}


write_conf() {
  FAKE_HOST=$1

cat <<EOF > /etc/dnsmasq.d/db.faked-zones.dns
address=/pi.fakes.local/$FAKE_HOST
address=/isos.$MY_DOMAIN/$FAKE_HOST
address=/iso.$MY_DOMAIN/$FAKE_HOST
address=/fedora.$MY_DOMAIN/$FAKE_HOST
address=/fedora/$FAKE_HOST
EOF

for curHost in `grep http /etc/apt-cacher-ng/* | awk -F "://" '{ print $2 }' | awk -F "/" '{ print $1 }' | grep -v ":" `; do
	echo "address=/${curHost}/$FAKE_HOST" >> /etc/dnsmasq.d/db.faked-zones.dns
done;

}

# Include pxe-install defaults if available
if [ -r /etc/default/pxe-install ]; then
        . /etc/default/pxe-install
else    
        usage
fi


[ $# -ne 0 ] && usage
FAKE_HOST=$1

write_conf $MY_IP


