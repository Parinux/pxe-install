#!/bin/sh

usage() {
    cat <<EOF >&2
$@
Usage: $0 
Use $0 to automatically write dns fake hosts file
( values are fetched from /etc/default/pxe-parinux )
EOF
    exit 1
}


write_conf() {
  FAKE_HOST=$1

cat <<EOF > /etc/bind/db.faked-zones.dns
\$TTL    604800
fakes.local.       IN      SOA     fakes.local. root.localhost. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL

fakes.local.       IN      NS     pi2.fakes.local.
pi2.fakes.local.   IN A $FAKE_HOST
isos.parinux.org   IN A $FAKE_HOST
iso   IN A $FAKE_HOST
isos	IN A $FAKE_HOST
fedora   IN A $FAKE_HOST

EOF

for curHost in `grep http /etc/apt-cacher-ng/* | awk -F "http://" '{ print $2 }' | awk -F "/" '{ print $1 }' | grep -v ":" `; do
	echo "${curHost} IN     A       $FAKE_HOST" >> /etc/bind/db.faked-zones.dns
	echo "*.${curHost} IN     A       $FAKE_HOST" >> /etc/bind/db.faked-zones.dns
done;

}

# Include pxe-install defaults if available
if [ -r /etc/default/pxe-install ]; then
        . /etc/default/pxe-install
else    
        usage
fi


[ $# -ne 0 ] && usage
FAKE_HOST=$1

write_conf $MY_IP


