#!/bin/bash

usage() {
    cat <<EOF >&2
$@
Usage: $0
Use $0 to automatically write dhcpd config file
( values are fetched from /etc/default/pxe-parinux )
EOF
    exit 1
}


write_conf() {
  NETWORK_RANGE=$1
  GATEWAY=$2
  DNS=$3 
  DOMAIN=$4 

  SUBNET=$(echo $NETWORK_RANGE | awk -F "/" '{ print $1 }')
  NETMASK=$(ipcalc $NETWORK_RANGE | grep Netmask | awk '{ print $2 }')
  BROADCAST=$(ipcalc $NETWORK_RANGE | grep Broadcast | awk '{ print $2 }')
  START_IP=$(ipcalc $NETWORK_RANGE | grep HostMin | awk '{ print $2"00" }')
  END_IP=$(ipcalc $NETWORK_RANGE | grep HostMax | awk '{ print $2 }')

cat <<EOF > /etc/dhcp/dhcpd.conf
ddns-update-style none;
authoritative;
log-facility local7;

option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;

option architecture-type code 93 = unsigned integer 16;

subnet $SUBNET netmask $NETMASK {
  range $START_IP $END_IP;
  option domain-name-servers $DNS;
  
  option domain-name "$DOMAIN";
  option routers $GATEWAY;
  option broadcast-address $BROADCAST;
  default-lease-time 172800;
  max-lease-time 172800;
  filename "pxelinux.0";
  server-name "$DNS";

  class "pxeclients" {
          match if substring (option vendor-class-identifier, 0, 9)
                  = "PXEClient";
          next-server $DNS;

          if option architecture-type = 00:02 {
                  filename "elilo.efi";
          } else if option architecture-type = 00:06 {
                  filename "bootia32.efi";
          } else if option architecture-type = 00:07 {
                  filename "bootx64.efi";
                  #filename "grubnetx64.efi.signed";
          } else {
                  filename "pxelinux.0";
          }
  }


}
EOF


}



[ $# -ne 0 ] && usage

# Include pxe-install defaults if available
if [ -r /etc/default/pxe-install ]; then
        . /etc/default/pxe-install
else
	usage
fi

NETWORK_RANGE=$MY_NETWORK_RANGE
GATEWAY=$MY_GATEWAY
DNS=$MY_IP
DOMAIN=$MY_DOMAIN

write_conf $NETWORK_RANGE $GATEWAY $DNS $DOMAIN


